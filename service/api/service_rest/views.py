from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.core.exceptions import ObjectDoesNotExist
from .models import Technician, Appointment
import json


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
        ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }

    def get_extra_data(self, o):
        return {
            'technician':
            o.technician.first_name + " " + o.technician.last_name
            }


class StatusEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "status",
    ]


@require_http_methods(["GET", "POST"])
def list_technicians(request, id=None):
    if request.method == "GET":
        if id is None:
            technicians = Technician.objects.all()
        else:
            technicians = Technician.objects.filter(id=id)
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def technician_detail(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except ObjectDoesNotExist:
            return JsonResponse(
                {'error': 'Technician does not exist'},
                status=404
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=id)
            content['technician'] = Technician.objects.get(
                id=content['technician']
                )
        except Technician.DoesNotExist:
            return JsonResponse(
                {'error': 'Technician does not exist'},
                status=400
            )
        Technician.objects.filter(id=id).update(**content)
        Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def list_appointments(request, id=None):
    if request.method == "GET":
        if id is None:
            appointments = Appointment.objects.all()
        else:
            appointments = Appointment.objects.filter(id=id)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technitian_id = content["technician"]
            technician = Technician.objects.get(id=technitian_id)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician does not exist"},
                status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
            safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def appointment_detail(request, id):
    appointment = Appointment.objects.get(id=id)
    if request.method == "GET":
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content['technician'])
            content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {'error': 'Technician does not exist'},
                status=400
            )
        Appointment.objects.filter(id=id).update(**content)
        Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    if request.method == "PUT":
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=StatusEncoder,
            safe=False
        )


@require_http_methods(["PUT"])
def finish_appointment(request, id=id):
    appointment = Appointment.objects.get(id=id)
    if request.method == "PUT":
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=StatusEncoder,
            safe=False
        )
