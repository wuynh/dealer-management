from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=30, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    employee_id = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.first_name + " " + self.last_name


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=150)
    status = models.CharField(max_length=30, default="Created")
    vin = models.CharField(max_length=30)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
        related_name="appointments"
        )

    def __str__(self):
        return self.customer + " " + self.reason

    def cancel(self):
        self.status = "Cancelled"
        self.save()

    def finish(self):
        self.status = "Finished"
        self.save()
