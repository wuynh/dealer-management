# CarCar

Team:

* Diana Ko - Sales
* Quynh Le - Service
## Instructions to Get Started
<mark>Make sure to have Git, Docker, and Insomia to access the end-points.</mark>
1. Fork this repository and clone it onto your computer with "git clone <<repository.url>>"
2. Run these commands in terminal to build and run the project:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- Important! Make sure all containers are running. If you are encountering any issues, you can stop the containers with `control` + `c` and restart with `docker-compose up` again.
- The project can be access at: http://localhost:3000/

## Design
![alt text](project-beta-diagram.png "Project Beta Diagram")

## Microservice Ports
Front-end application: http://localhost:3000/ <br>
Inventory API: http://localhost:8100/ <br>
Service API: http://localhost:8080/ <br>
Sales API: http://localhost:8090/ <br>

### Inventory
The inventory drop down menu allows you to:
- View lists of Manifacturers, Models, and Automobiles (Sold/Unsold)
- Add new Manifacturers, Models, and Automobiles

| Url Paths | Method | Action | Required Field(s) |
|---|---|---|---|
| `automobiles/` | GET | List all Automobiles |
| `automobiles/add/` | POST | Form to add Automobile | Color, Year, Vin, Model Choice|
| `vehicle/` | GET | List all Vehicle Models |
| `vehicle/add/` | POST | Form to add Vehicle Model | Name, Picture Url, Manufacturer Choice|
| `manufacturer/` | GET | List all Manufacturers |
| `manufacturer/add/` | POST | Form to add Manufacturer | Manufacturer Name |

### RestfulAPIs endpoints:
<br>

**Manufacturers:**
<br>

`GET` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all manufacturers | GET | http://localhost:8100/api/manufacturers/
| See specific manufacturer details | GET | http://localhost:8100/api/manufacturers/<int:id>/
<details>
<summary>JSON</summary>
List all manufacturers:<br>
Response:

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "BMW"
		},
	]
}
```
See specific manufacturer details:<br>
Example: http://localhost:8100/api/manufacturers/1/<br>
Response:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "BMW"
}
```
</details>
<br>

`POST` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Add new manufacturer | POST | http://localhost:8100/api/manufacturers/
<details>
<summary>JSON</summary>
Sent:

```
{
	"name": "Honda"
}
```
Response:

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Honda"
}
```
</details>
<br>

`PUT` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Update manufacturer | PUT | http://localhost:8100/api/manufacturers/<int:id>/
<details>
<summary>JSON</summary>
Sent:

```
{
	"name": "Porsche"
}
```
Response:

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Porsche"
}
```
</details>
<br>

`DELETE` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Delete manufacturer | DELETE | http://localhost:8100/api/manufacturers/<int:id>/
<details>
<summary>JSON</summary>
Response:

```
{
	"id": null,
	"name": "BMW"
}
```
</details>
<br>

**Models:**
<br>

`GET` methods:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all models | GET | http://localhost:8100/api/models/
| See specific model details | GET | http://localhost:8100/api/models/<int:id>/
<details>
<summary>JSON</summary>
List all models:<br>
Response:

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "M3",
			"picture_url": "https://www.motortrend.com/uploads/sites/5/2020/06/2021-BMW-M3-Competition-1.jpg?fit=around%7C875:492",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "BMW"
			}
		},
	]
}
```
See specific model details:<br>
Example: http://localhost:8100/api/models/1/<br>
Response:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "M3",
	"picture_url": "https://www.motortrend.com/uploads/sites/5/2020/06/2021-BMW-M3-Competition-1.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "BMW"
	}
}
```
</details>
<br>

`POST` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Add new model | POST | http://localhost:8100/api/models/
<details>
<summary>JSON</summary>
Sent:

```
{
	"name": "911 GT3 RS",
	"picture_url": "https://www.motortrend.com/uploads/sites/5/2020/06/2021-Porsche-911-GT3-RS-1.jpg?fit=around%7C875:492",
	"manufacturer": 2
}
```
Response:

```
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "911 GT3 RS",
	"picture_url": "https://www.motortrend.com/uploads/sites/5/2020/06/2021-Porsche-911-GT3-RS-1.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Porsche"
	}
}
```
</details>
<br>

`PUT` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Update model | PUT | http://localhost:8100/api/models/<int:id>/
<details>
<summary>JSON</summary>
Sent:

```
{
	"name": "911 Dakar",
	"picture_url": "https://carwow-uk-wp-1.imgix.net/porsche-911-dakar-rally-design-pack-front-4.jpg?auto=format&cs=tinysrgb&fit=crop&h=800&ixlib=rb-1.1.0&q=60&w=1600",
	"manufacturer": 2
}
```
Response:

```
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "911 Dakar",
	"picture_url": "https://carwow-uk-wp-1.imgix.net/porsche-911-dakar-rally-design-pack-front-4.jpg?auto=format&cs=tinysrgb&fit=crop&h=800&ixlib=rb-1.1.0&q=60&w=1600",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Porsche"
	}
}
```
</details>
<br>

`DELETE` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Delete model | DELETE | http://localhost:8100/api/models/<int:id>/
<details>
<summary>JSON</summary>
Response:

```
{
	"id": null,
	"name": "911 Dakar",
	"picture_url": "https://carwow-uk-wp-1.imgix.net/porsche-911-dakar-rally-design-pack-front-4.jpg?auto=format&cs=tinysrgb&fit=crop&h=800&ixlib=rb-1.1.0&q=60&w=1600",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Porsche"
	}
}
```
</details>
<br>

**Automobiles:**
<br>

`GET` methods:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all automobiles | GET | http://localhost:8100/api/automobiles/
| See specific automobile details | GET | http://localhost:8100/api/automobiles/<:vin>/
<details>
<summary>JSON</summary>
List all automobiles:<br>
Response:

```
{
	"autos": [
		{
			"href": "/api/automobiles/PK0263ILS788/",
			"id": 1,
			"color": "White",
			"year": 2023,
			"vin": "PK0263ILS788",
			"model": {
				"href": "/api/models/2/",
				"id": 2,
				"name": "911 GT3 RS",
				"picture_url": "https://www.motortrend.com/uploads/2023/03/2023-porsche-911-gt3-rs-04.jpg?fit=around%7C875:492",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Porsche"
				}
			},
			"sold": false
		},
	]
}
```

See specific automobile details:<br>
Example: http://localhost:8100/api/automobiles/PK0263ILS788/<br>
Response:

```
{
	"href": "/api/automobiles/PK0263ILS788/",
	"id": 1,
	"color": "White",
	"year": 2023,
	"vin": "PK0263ILS788",
	"model": {
		"href": "/api/models/2/",
		"id": 1,
		"name": "911 GT3 RS",
		"picture_url": "https://www.motortrend.com/uploads/2023/03/2023-porsche-911-gt3-rs-04.jpg?fit=around%7C875:492",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Porsche"
		}
	},
	"sold": false
}
```
</details>
<br>

`POST` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create automobile | POST | http://localhost:8100/api/automobiles/
<details>
<summary>JSON</summary>
Sent:

```
{
  "color": "green",
  "year": 2022,
  "vin": "2B3CC5FI73T935677",
  "model_id": 2
}
```
Response:

```
{
	"href": "/api/automobiles/2B3CC5FI73T935677/",
	"id": 1,
	"color": "green",
	"year": 2022,
	"vin": "2B3CC5FI73T935677",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "911 GT3 RS",
		"picture_url": "https://www.motortrend.com/uploads/2023/03/2023-porsche-911-gt3-rs-04.jpg?fit=around%7C875:492",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Porsche"
		}
	},
	"sold": false
}
```
</details>
<br>

`PUT` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Update automobile | PUT | http://localhost:8100/api/automobiles/<:vin>/
<details>
<summary>JSON</summary>
Sent:

```
{
  "color": "blue",
  "sold": true
}
```
Response:

```
{
	"href": "/api/automobiles/2B3CC5FI73T935677/",
	"id": 1,
	"color": "blue",
	"year": 2022,
	"vin": "2B3CC5FI73T935677",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "911 GT3 RS",
		"picture_url": "https://www.motortrend.com/uploads/2023/03/2023-porsche-911-gt3-rs-04.jpg?fit=around%7C875:492",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Porsche"
		}
	},
	"sold": true
}
```
</details>
<br>

`DELETE` method:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Delete automobile | DELETE | http://localhost:8100/api/automobiles/<:vin>/
<details>
<summary>JSON</summary>
Response:

```
{
	"href": "/api/automobiles/2B3CC5FI73T935677/",
	"id": null,
	"color": "blue",
	"year": 2022,
	"vin": "2B3CC5FI73T935677",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "911 GT3 RS",
		"picture_url": "https://www.motortrend.com/uploads/2023/03/2023-porsche-911-gt3-rs-04.jpg?fit=around%7C875:492",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Porsche"
		}
	},
	"sold": true
}
```
</details>
<br>

## Service microservice

The service microservice handles service-related functionalities and is an extension of inventory microservice which it used to poll in data. It consists of the following models:

**AutomobileVO**:
- This model is an instance of the inventory microservices, which will be used to poll and store vin data from the inventory database every 60 seconds.

**Technician**:
- This handles and store all technicians data with the "first_name", "last_name", and "employee_id" fields.

**Appointment**:
- This handles and store all service appointment data with the "date_time", "reason", "status", "vin", "customer" fields. It also includes a "technican" field as a foreign key to the Technican model.

**RestfulAPIs**:
- Technician have RESTful APIs methods such as `GET`, `POST`, and `DELETE`.
- Appointment have RESTful APIs methods such as `GET`, `POST`, `PUT`, and `DELETE`.

### Technician

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| See a specific technican details | GET | http://localhost:8080/api/technicians/<int:id>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/<int:id>/

<br>

**List technicians**:
This `GET` method will return a list of all technicians.
<details>
<summary>JSON</summary>

```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Bob",
			"last_name": "John",
			"employee_id": 123
		},
		{
			"id": 2,
			"first_name": "Rupa",
			"last_name": "Simon",
			"employee_id": 12
		},
	]
}
```
</details>
<br>

**Technician details**: 
This `GET` method will return a specific technician details, use an id from the technicians list to access that specific technician.<br>
Example: http://localhost:8080/api/technicians/1/
<details>
<summary>JSON</summary>

```
{
	"id": 1,
	"first_name": "Bob",
	"last_name": "John",
	"employee_id": 123
}
```
</details>
<br>

**Create a technician**:
This `POST` method will create a new technician with the data input from JSON body with the following format:
<details>
<summary>JSON</summary>
Sent:

```
{
	"first_name": "Kyle",
	"last_name": "Lee",
	"employee_id": 324
}
```
This method will return this response if it is successfully created:
```
{
	"id": 3,
	"first_name": "Kyle",
	"last_name": "Lee",
	"employee_id": 324
}
```
</details>
<br>

**Delete a specific technician**:
This `DELETE` method will delete and remove a specific technicians from our database 😓 <br>
Example: http://localhost:8080/api/technicians/3/
<details>
<summary>JSON</summary>
If successful, it will return:

```
{
	"deleted": true
}

```
</details>
<br>

### Appointment
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Set appointment status to "Canceled" | PUT | http://localhost:8080/api/appointments/<int:id>/cancel/
| Set appointment status to "Finished" | PUT | http://localhost:8080/api/appointments/<int:id>/finish/

<br>

**List appointments**:
This `GET` method will return a list of all appointments.
<details>
<summary>JSON</summary>
Response:

```
{
	"appointments": [
		{
			"id": 1,
			"date_time": "2023-06-10T10:00:00+00:00",
			"reason": "Regular maintenance",
			"status": "Finished",
			"vin": "ABC123",
			"customer": "John Doe",
			"technician": "Bob John"
		},
		{
			"id": 2,
			"date_time": "2023-06-16T11:15:00+00:00",
			"reason": "Oil change",
			"status": "Cancelled",
			"vin": "J7253P012356",
			"customer": "Perry Moni",
			"technician": "Rupa Simon"
		},
	]
}
```
</details>
<br>


**Appointment details**: 
This `GET` method will return a specific appointment details, use an id from the appointment list to access that specific appointment.<br>
Example: http://localhost:8080/api/appointments/1/
<details>
<summary>JSON</summary>
Response:

```
{
	"id": 1,
	"date_time": "2023-06-10T10:00:00+00:00",
	"reason": "Regular maintenance",
	"status": "Finished",
	"vin": "ABC123",
	"customer": "John Doe",
	"technician": "Bob John"
}
```
</details>
<br>

**Create an appointment**:
This `POST` method will create a new appointment with the data input from JSON body with the following format:
<details>
<summary>JSON</summary>
Sent:

```
{
  "date_time": "2023-06-22T13:00:00",
  "reason": "Smog Check",
  "vin": "J531498TDI93456",
  "customer": "Terry Jonah",
  "technician": 1
}
* date_time format is "yyyy-mm-ddThh:mm:ss".
* status of appointment is set to "Created" automatically when first created.
```
This method will return this response if it is successfully created:
```
{
	"appointment": {
		"id": 3,
		"date_time": "2023-06-22T13:00:00",
		"reason": "Smog Check",
		"status": "Created",
		"vin": "J531498TDI93456",
		"customer": "Terry Jonah",
		"technician": "Bob John"
	}
}
```
</details>
<br>

**Change appointment status**:
This `PUT` method will change appointment status to "Cancelled" or "Finished" accordiningly.
<br>
Example:
- Send `PUT` request to http://localhost:8080/api/appointments/3/cancel/ to set appointment at id 3 status to "Cancelled".
- Send `PUT` request to http://localhost:8080/api/appointments/3/finish/ to set appointment at id 3 status to "Finished".<br>

<details>
<summary>JSON</summary>
Cancel response:

```
{
	"id": 3,
	"date_time": "2023-06-22T13:00:00+00:00",
	"status": "Cancelled"
}
```
Finish response:

```
{
	"id": 3,
	"date_time": "2023-06-22T13:00:00+00:00",
	"status": "Finished"
}
```
</details>

*Note that Navbar only contains links to `GET` and `POST` methods for all models.*


## Sales microservice

The Sales microservice manages creating, deleting, and recieving data instances created from the four models.The Sales microservice conatins 4 models: Customer, Salesperson, Sale, *AutomobileVO*.

The *AutomobileVO* is a duplicate of our Inventory Automobile model, and only take in one field `vin`. We use polling to scout for data in our Inventory database, and populate our sales database with the same automobile vins.

Each model, except for the *AutomobileVO*, has two view functions one that handles the methods `GET` and `POST` and another that handles `DELETE`.

Everything request for the sales microservice will be managed at `http://localhost:8090/api/` + specified url paths.
### Sales
The sales drop down menu allows you to:
- View lists of Salespeople, Customers, and Sales/Individual Sale History
- Create new Salespeople, Customers, and Sales

| Url Paths | Method | Action | Required Field(s) |
|---|---|---|---|
| `salespeople/` | GET | List all Salespeople |
| `salespeople/add/` | POST | Form to add Salesperson | First Name, Last Name, Employee ID|
| `customer/` | GET | List all Customers |
| `customer/add/` | POST | Form to add Customer | First Name, Last Name, Address, Phone|
| `sales/` | GET | List all Sales |
| `sales/add/` | POST | Form to add Sale| VIN, Salesperson, Customer, Price |
| `sales/history/` | GET | List of Employee Sale History |

#### Salespeople


The Salesperson Model contains 3 required fields:

- first_name
- last_name
- employee_id

*The "POST" method requires all three fields to be present in the request in order process successfully. It will return an object with the inputted information and a new id field.*


| Action | Method | Url Path |
|---|---|---|
| Create Salesperson | POST | `salespeople/`|
<details>
<summary>JSON</summary>
<br>

Sent:
```
{
    "first_name": "Bob",
    "last_name": "Saget",
    "employee_id": "BSag",
}
```
Returns:
```
{
    "first_name": "Bob",
    "last_name": "Saget",
    "employee_id": "BSag",
    "id": 2
}
```
</details>

*The "DELETE" method takes the salesperson id (not the emloyee_id), and deletes that instance of the employee from the database. It returns an object stating whether or not the salesperson has been deleted*

| Action | Method | Url Path |
|---|---|---|
| Delete Salesperson | DELETE | `salespeople/:id/`|
<details>
<summary>JSON</summary>

Returns:
```
{
    {"Employee Terminated": True}
}
```
</details>

*The "GET" method returns an object containing the key: "salespeople" and the value: [list of all salespeople on record].*
| Action | Method | Url Path |
|---|---|---|
| List all Salespeople | GET | `salespeople/`|
<details>
<summary>JSON</summary>
<br>

Returns:
```
{
"salespeople": [
    {
        "first_name": "Bob",
        "last_name": "Smith",
        "employee_id": "BSmith",
        "id": 1
    },
    {
        "first_name": "Frank",
        "last_name": "Smith",
        "employee_id": "FSmith",
        "id": 2
    }
]
}
```
</details>

#### Customers

The Customer Model contains 4 required fields:

- first_name
- last_name
- address
- phone_number

*The customer "POST" method requires all four fields to be present in order process successfully. It will return an object with the input information and a new id field.*


| Action | Method | Url Path |
|---|---|---|
| Create Customer | POST | `customers/`|
<details>
<summary>JSON</summary>
<br>

Sent:
```
{
	"first_name": "Tony",
	"last_name": "Lee",
	"address": "123 Hollywood Blvd, CA 90210",
	"phone_number": "323-443-8493",
}
```
Returns:
```
{
	"first_name": "Tony",
	"last_name": "Lee",
	"address": "123 Hollywood Blvd, CA 90210",
	"phone_number": "323-443-8493",
	"id": 1
}
```
</details>

*The customer "DELETE" method takes the customer id, and deletes that instance of the customer. It returns an object stating whether or not they have been deleted*

| Action | Method | Url Path |
|---|---|---|
| Delete Customer | DELETE | `customers/:id/`|
<details>
<summary>JSON</summary>

Returns:
```
{
    {"Customer Deleted": True}
}
```
</details>

*The customers "GET" method returns an object containing the key: "customers" and the value: [list of all customers on record].*

| Action | Method | Url Path |
|---|---|---|
| List all Customers | GET | `customers/`|
<details>
<summary>JSON</summary>
<br>

Returns:
```
{
"customers":[
    {
            "first_name": "Sarah",
            "last_name": "Smith",
            "address": "123 Crossing Blvd, CA 90210",
            "phone_number": "323-453-8493",
            "id": 1
        },
        {
            "first_name": "Saul",
            "last_name": "Goodman",
            "address": "124 Crossing Blvd, CA 90210",
            "phone_number": "800-443-8456",
            "id": 2
        }
    ]
}
```
</details>
The Sale Model contains 4 required fields:

- Price
- id (Customer)
- Employee_id (Salesperson)
- Vin (Automobile)

*The sale "POST" method requires all four fields to be present, and valid, in order process successfully. It will return an object with the complete information from user input, a new id field, and the key: "sold" with a value: "true" in our automobile object.*


| Action | Method | Url Path |
|---|---|---|
| Create Sale | POST | `sales/`|
<details>
<summary>JSON</summary>
<br>

Sent:
```
{
	"price": 200000.00,
	"customer": 5,
	"salesperson": "CJay",
	"automobile": "12345DIANA2000"
}
```
Returns:
```
{
	"price": 200000.0,
	"customer": {
		"first_name": "Hank",
		"last_name": "Gomez",
		"address": "123 Crossing Blvd, CA 90210",
		"phone_number": "323-453-8493",
		"id": 5
	},
	"salesperson": {
		"first_name": "Bobby",
		"last_name": "Jay",
		"employee_id": "BJay",
		"id": 2
	},
	"automobile": {
		"vin": "12345DIANA2000",
		"sold": true
	},
	"id": 1
}
```
</details>

*The sale "DELETE" method takes the sales id, and deletes that instance of the sale. It returns an object stating whether or not the sale has been deleted.*

| Action | Method | Url Path |
|---|---|---|
| Delete Sale | DELETE | `sales/:id/`|
<details>
<summary>JSON</summary>

Returns:
```
{
    {"Sale Removed": True}
}
```
</details>

*The sales "GET" method returns an object containing the key: "sales" and the value: [list of all sales on record].*

| Action | Method | Url Path |
|---|---|---|
| List all Sales | GET | `sales/`|
<details>
<summary>JSON</summary>
<br>

Returns:
```
{
"sales":[
    {
        "price": 300000.0,
        "customer": {
            "first_name": "Hank",
            "last_name": "Gomez",
            "address": "123 Crossing Blvd, CA 90210",
            "phone_number": "323-453-8493",
            "id": 5
        },
        "salesperson": {
            "first_name": "Bobby",
            "last_name": "Jay",
            "employee_id": "BJay",
            "id": 2
        },
        "automobile": {
            "vin": "12345DIANA2000",
            "sold": true
        },
        "id": 1
    },
        {
        "price": 83000,
        "customer": {
            "first_name": "Ron",
            "last_name": "Coleman",
            "address": "124 Hollywood Blvd, CA 90210",
            "phone_number": "800-443-8456",
            "id": 6
        },
        "salesperson": {
            "first_name": "Mike",
            "last_name": "Smith",
            "employee_id": "MSmith",
            "id": 8
        },
        "automobile": {
            "vin": "091TUNDRA0192XYWX",
            "sold": true
        },
        "id": 15
    }
    ]
}
```
</details>
