import React, { useState, useEffect } from "react";
import "./styles.css";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [search, setSearch] = useState("");
  const [filterSearch, setFilterSearch] = useState([search]);

  const getAppointments = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      const data = await response.json();
      setAppointments(data.appointments);
    } catch (error) {
      console.error(error);
    }
  };

  const statusBadge = (status) => {
    if (status === "Finished") {
      return <span className="badge badge-pill bg-success">{status}</span>;
    } else if (status === "Cancelled") {
      return <span className="badge badge-pill bg-danger">{status}</span>;
    } else {
      return <span className="badge badge-pill bg-warning">{status}</span>;
    }
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  useEffect(() => {
    const filtered = appointments.filter((appointment) =>
      appointment.vin.toLowerCase().includes(search.toLowerCase())
    );
    setFilterSearch(filtered);
  }, [search, appointments]);

  useEffect(() => {
    getAppointments();
  }, []);

  return (
    <div className="mt-4">
      <h1 className="list">Service History</h1>
      <div className="searchbar">
        <form className="search-form d-flex">
          <input
            className="form-control me-2"
            type="search"
            placeholder="Search by VIN..."
            aria-label="Search"
            onChange={handleSearch}
            value={search}
          />
          <button className="btn btn-success" type="submit">
            Search
          </button>
        </form>
      </div>
      <br></br>
      <table className="table table-center">
        <thead className="table-light text-center">
          <tr className="align-middle text-center">
            <th scope="col">VIN</th>
            <th scope="col">Customer Name</th>
            <th scope="col">Date-Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {filterSearch.map((appointment, index) => (
            <tr className="align-middle text-center" key={index}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician}</td>
              <td>{appointment.reason}</td>
              <td>{statusBadge(appointment.status)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
