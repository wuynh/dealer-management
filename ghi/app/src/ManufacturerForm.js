import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./styles.css";

function ManufacturerForm() {
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");
  const [success, setSuccess] = useState(false);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      console.error(error);
    }
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;

    const url = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      setSuccess(true);
      setName("");
    } else {
      console.error(response);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    const successAlert = document.getElementById("success");
    const form = document.getElementById("create-manufacturer-form");
    if (success) {
      form.classList.add("d-none");
      successAlert.classList.remove("d-none");
    } else {
      form.classList.remove("d-none");
      successAlert.classList.add("d-none");
    }
  }, [success]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form id="create-manufacturer-form" onSubmit={handleSubmit}>
            <h1>Add a Manufacturer</h1>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Name
              </label>
              <input
                required
                type="text"
                name="name"
                id="name"
                value={name}
                onChange={handleNameChange}
                className="form-control"
              />
            </div>
            <div>
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <div className="alert alert-success d-none" id="success">
          Successfully added a manufacturer!{" "}
          <NavLink to="/manufacturers" className="alert-link">
            View all manufacturers.
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
