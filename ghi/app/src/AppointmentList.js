import React, { useState, useEffect } from "react";
import './styles.css'

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [vip, setVip] = useState(false);

  const getAppointments = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/appointments");
      if (response.ok) {
        const data = await response.json();
        const created = data.appointments.filter(
          (appointment) => appointment.status === "Created"
        );
        const vipAppointments = await Promise.all(
          created.map(async (appointment) => {
            const vipStatus = await getVipStatus(appointment.vin);
            return { ...appointment, vip: vipStatus };
          })
        );
        setAppointments(vipAppointments);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const getVipStatus = async (vin) => {
    try {
      const response = await fetch("http://localhost:8100/api/automobiles");
      if (response.ok) {
        const data = await response.json();
        const matchedVin = data.autos.find((auto) => auto.vin === vin);
        return matchedVin ? "VIP" : "Regular";
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleCancel = async (id) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/cancel/`,
        {
          method: "PUT",
        }
      );
      if (response.ok) {
        setAppointments((prevAppointments) =>
          prevAppointments.filter((appointment) => appointment.id !== id)
        );
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleFinish = async (id) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/finish/`,
        {
          method: "PUT",
        }
      );
      if (response.ok) {
        setAppointments((prevAppointments) =>
          prevAppointments.filter((appointment) => appointment.id !== id)
        );
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getAppointments();
  }, []);

  return (
    <div className="mt-4">
      <h1 className="list">Service Appointments</h1>
      <table className="table table-center">
        <thead className="table-primary text-center">
          <tr className="align-middle text-center">
            <th scope="col">VIN</th>
            <th scope="col">VIP Status</th>
            <th scope="col">Customer Name</th>
            <th scope="col">Date-Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr className="align-middle text-center" key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>
                {appointment.vip === "VIP" ? (
                  <span className="badge badge-pill bg-success">VIP</span>
                ) : (
                  <span className="badge badge-pill bg-warning">Regular</span>
                )}
              </td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician}</td>
              <td>{appointment.reason}</td>
              <td>
                <div className="d-flex justify-content-evenly">
                  <button
                    className="btn btn-success btn-sm"
                    onClick={() => handleFinish(appointment.id)}
                  >
                    Finish
                  </button>
                  <button
                    className="btn btn-danger btn-sm"
                    onClick={() => handleCancel(appointment.id)}
                  >
                    Cancel
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
