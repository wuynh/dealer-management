import React, { useState, useEffect } from "react";
import "./styles.css";

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="mt-4">
      <h1 className="list">Technicians</h1>
      <table className="table table-centered">
        <thead className="table-light text-center">
          <tr className="align-middle text-center">
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr className="align-middle text-center" key={technician.id}>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>{technician.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
