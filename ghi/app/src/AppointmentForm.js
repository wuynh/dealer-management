import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./styles.css";

function AppointmentForm() {
  const [vin, setVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [dateTime, setDateTime] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");
  const [success, setSuccess] = useState(false);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      const data = await response.json();
      setTechnicians(data.technicians);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.date_time = dateTime;
    data.technician = technician;
    data.reason = reason;

    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newAppointment = await response.json();
        setSuccess(true);
        setVin("");
        setCustomer("");
        setDateTime("");
        setTechnician("");
        setReason("");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleVinChange = (event) => {
    setVin(event.target.value);
  };

  const handleCustomerChange = (event) => {
    setCustomer(event.target.value);
  };

  const handleDateTimeChange = (event) => {
    setDateTime(event.target.value);
  };

  const handleTechnicianChange = (event) => {
    setTechnician(event.target.value);
  };

  const handleReasonChange = (event) => {
    setReason(event.target.value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    const successAlert = document.getElementById("success");
    const form = document.getElementById("create-appointment-form");
    if (success) {
      form.classList.add("d-none");
      successAlert.classList.remove("d-none");
    } else {
      form.classList.remove("d-none");
      successAlert.classList.add("d-none");
    }
  }, [success]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form id="create-appointment-form" onSubmit={handleSubmit}>
            <h1>Create a service appointmnent</h1>
            <div className="mb-3">
              <label htmlFor="vin" className="form-label">
                Automobile VIN
              </label>
              <input
                required
                type="text"
                name="vin"
                id="vin"
                value={vin}
                onChange={handleVinChange}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="customer" className="form-label">
                Customer Name
              </label>
              <input
                required
                type="text"
                name="customer"
                id="customer"
                value={customer}
                onChange={handleCustomerChange}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="date_time" className="form-label">
                Date-Time
              </label>
              <input
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                value={dateTime}
                onChange={handleDateTimeChange}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="technician" className="form-label">
                Technician
              </label>
              <select
                required
                name="technician"
                id="technician"
                value={technician}
                onChange={handleTechnicianChange}
                className="form-control"
              >
                <option value="">Select a technician</option>
                {technicians.map((technician) => (
                  <option value={technician.id} key={technician.id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="reason" className="form-label">
                Reason
              </label>
              <input
                required
                type="text"
                name="reason"
                id="reason"
                value={reason}
                onChange={handleReasonChange}
                className="form-control"
              />
            </div>
            <div>
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <div className="alert alert-success d-none" id="success">
          Successfully created an appointment!{" "}
          <NavLink to="/appointments" className="alert-link">
            View all appointments.{" "}
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
