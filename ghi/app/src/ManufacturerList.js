import React, { useState, useEffect } from "react";
import "./styles.css";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);
  const [search, setSearch] = useState("");
  const [filterSearch, setFilterSearch] = useState([search]);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const sortedManufacturers = [...manufacturers].sort((a, b) =>
    a.name.localeCompare(b.name)
  );

  useEffect(() => {
    const results = sortedManufacturers.filter((manufacturer) =>
      manufacturer.name.toLowerCase().includes(search.toLowerCase())
    );
    setFilterSearch(results);
  }, [search, manufacturers]);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="mt-4">
      <h1 className="list">Manufacturers</h1>
      <div className="searchbar">
        <form className="search-form d-flex">
          <input
            className="form-control me-2"
            type="search"
            placeholder="Search manufacturer by name..."
            aria-label="Search"
            onChange={handleSearch}
            value={search}
          />
          <button className="btn btn-success" type="submit">
            Search
          </button>
        </form>
      </div>
      <br></br>
      <table className="table table-centered">
        <thead className="table-primary text-center">
          <tr className="align-middle text-center">
            <th scope="col">#</th>
            <th scope="col">Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {filterSearch.map((manufacturer, index) => (
            <tr className="align-middle text-center" key={index}>
              <td className="text-center">{index + 1}</td>
              <td className="text-center">{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
