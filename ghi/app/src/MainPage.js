import React, { useEffect, useState } from "react";
import "./MainPage.css";

function MainPage() {
  const [word, setWord] = useState("Inventory");
  const words = ["Inventory", "Sales", "Services"];
  let wordIndex = 0;

  useEffect(() => {
    const intervalId = setInterval(changeWord, 4000);
    return () => clearInterval(intervalId);
  }, []);

  function changeWord() {
    wordIndex = (wordIndex + 1) % words.length;
    setWord(words[wordIndex]);
  }

  return (
    <div className="content">
      <div className="text-content">
        <h1 className="display-5 fw-bold">
          The premiere solution for automobile dealership management! One stop
          shop for all. <span className="animated-text">{word}</span>
        </h1>
      </div>
      <div className="image-content">
        <img src="main.png" alt="cars" style={{ width: "600px" }} />
      </div>
      <div className="info-content margin-bottom">
        <div className="row">
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="gradient-text card-title">Inventory</h5>
                <p className="card-text">
                  Effortlessly manage your inventory with features such as
                  manufacturers list, add models, and automobiles. Keep track of
                  manufacturers, add new models, and view comprehensive details
                  of the vehicles in your inventory.
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="gradient-text card-title">Sales</h5>
                <p className="card-text">
                  Streamline your sales processes with features like sales list,
                  sales history, and customer management. Easily view and manage
                  sales records, track sales history, and maintain customer
                  information for efficient sales operations.
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="gradient-text card-title">Services</h5>
                <p className="card-text">
                  Optimize your service operations with a technicians list,
                  appointment management, and service appointment history.
                  Manage technicians and their skills, schedule and track
                  service appointments, and maintain a record of past service
                  appointments.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
