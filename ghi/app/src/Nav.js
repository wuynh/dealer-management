import { NavLink } from 'react-router-dom';
import './Nav.css';

function Nav() {
  return (
      <nav className="navbar navbar-expand-lg navbar-light d-flex align-items-center">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            <img src="https://www.svgrepo.com/show/513490/car.svg" alt="car" width="35" height="35" className="d-inline-block align-middle me-2" />
              CarCar
          </NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className='nav-item'>
                <NavLink className="nav-link" to="/">Home</NavLink>
              </li>
              <li className='nav-item dropdown'>
                <NavLink className="btn btn-secondary dropdown-toggle transparent-button" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">Inventory</NavLink>
                <ul className="dropdown-menu multi-column columns-3">
                  <div className="row">
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Manufacturers</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/manufacturers/add">Add a Manufacturer</NavLink>
                        </li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Models</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/models">Models List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/models/add">Add a Model</NavLink>
                        </li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Automobiles</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/automobiles/add">Add an Automobile</NavLink>
                        </li>
                      </ul>
                    </div>
                  </div>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <NavLink className="btn btn-secondary dropdown-toggle transparent-button" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">Sales</NavLink>
                <ul className="dropdown-menu multi-column columns-3">
                  <div className="row">
                    <div className="col-sm-4">
                        <ul className="multi-column-dropdown">
                          <li><h6 className="dropdown-header">Sales</h6></li>
                          <li>
                            <NavLink className="dropdown-item" to="/sales">Sales List</NavLink>
                          </li>
                          <li>
                            <NavLink className="dropdown-item" to="/sales/history">Sales History</NavLink>
                          </li>
                          <li>
                            <NavLink className="dropdown-item" to="/sales/add">Add a Sale</NavLink>
                          </li>
                        </ul>
                      </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Customers</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/customers">Customers List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/customers/add">Add a Customer</NavLink>
                        </li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Salespeople</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/salespeople">Salespeople List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/salespeople/add">Add a Salesperson</NavLink>
                        </li>
                      </ul>
                    </div>
                  </div>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <NavLink className="btn btn-secondary dropdown-toggle transparent-button" to="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">Services</NavLink>
                <ul className="dropdown-menu multi-column columns-3">
                  <div className="row">
                    <div className="col-sm-4">
                        <ul className="multi-column-dropdown">
                          <li><h6 className="dropdown-header">Technicians</h6></li>
                          <li>
                            <NavLink className="dropdown-item" to="/technicians">Technicians List</NavLink>
                          </li>
                          <li>
                            <NavLink className="dropdown-item" to="/technicians/add">Add a Technician</NavLink>
                          </li>
                        </ul>
                      </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Appointments</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/appointments">Appointments List</NavLink>
                        </li>
                        <li>
                          <NavLink className="dropdown-item" to="/appointments/add">Add an Appointment</NavLink>
                        </li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><h6 className="dropdown-header">Service</h6></li>
                        <li>
                          <NavLink className="dropdown-item" to="/services">Service History</NavLink>
                        </li>
                      </ul>
                    </div>
                  </div>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
  );
}

export default Nav;
