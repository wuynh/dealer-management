import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./styles.css";

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([]);
  const [manufacturer, setManufacturer] = useState("");
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [success, setSuccess] = useState(false);

  const fetchData = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      console.error(error);
    }
  };  

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.name = name;
    data.picture_url = pictureUrl;
    data.manufacturer_id = manufacturer;
    
    const url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newModel = await response.json();

        setSuccess(true);
        setName("");
        setPictureUrl("");
        setManufacturer("");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleManufacturerChange = (event) => {
    setManufacturer(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handlePictureUrlChange = (event) => {
    setPictureUrl(event.target.value);
  };

  useEffect(() => {
    const successAlert = document.getElementById("success");
    const form = document.getElementById("create-model-form");
    if (success) {
      form.classList.add("d-none");
      successAlert.classList.remove("d-none");
    } else {
      form.classList.remove("d-none");
      successAlert.classList.add("d-none");
    }
  }, [success]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form id="create-model-form" onSubmit={handleSubmit}>
            <h1>Add a Model</h1>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Model Name
              </label>
              <input
                required
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={name}
                onChange={handleNameChange}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="pictureUrl" className="form-label">
                Picture URL
              </label>
              <input
                required
                type="url"
                className="form-control"
                id="picture_url"
                name="picture_url"
                value={pictureUrl}
                onChange={handlePictureUrlChange}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="manufacturer" className="form-label">
                Manufacturer
              </label>
              <select
                required
                className="form-select"
                id="manufacturer_id"
                name="manufacturer_id"
                value={manufacturer}
                onChange={handleManufacturerChange}
              >
                <option value="">Select a manufacturer</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <button type="submit" className="btn btn-primary">
                Add Model
              </button>
            </div>
          </form>
        </div>
        <div className="alert alert-success d-none" id="success">
          Successfully added a model!{" "}
          <NavLink to="/models" className="alert-link">
            View all models.
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default ModelForm;
