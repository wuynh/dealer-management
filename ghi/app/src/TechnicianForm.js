import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./styles.css";

function TechnicianForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [success, setSuccess] = useState(false);

  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleEmployeeIdChange = (event) => {
    setEmployeeId(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const url = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newTechnician = await response.json();
        setSuccess(true);
        setFirstName("");
        setLastName("");
        setEmployeeId("");
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    const successAlert = document.getElementById("success");
    const form = document.getElementById("create-technician-form");
    if (success) {
      form.classList.add("d-none");
      successAlert.classList.remove("d-none");
    } else {
      form.classList.remove("d-none");
      successAlert.classList.add("d-none");
    }
  }, [success]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form id="create-technician-form" onSubmit={handleSubmit}>
            <h1>Add a technician</h1>
            <div className="mb-3">
              <label htmlFor="first_name" className="form-label">
                First Name
              </label>
              <input
                required
                type="text"
                name="first_name"
                id="first_name"
                value={firstName}
                onChange={handleFirstNameChange}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="last_name" className="form-label">
                Last Name
              </label>
              <input
                required
                type="text"
                name="last_name"
                id="last_name"
                value={lastName}
                onChange={handleLastNameChange}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="employee_id" className="form-label">
                Employee ID
              </label>
              <input
                required
                type="text"
                name="employee_id"
                id="employee_id"
                value={employeeId}
                onChange={handleEmployeeIdChange}
                className="form-control"
              />
            </div>
            <div>
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <div className="alert alert-success d-none" id="success">
          Successfully added a technician!{" "}
          <NavLink to="/technicians" className="alert-link">
            View all technicians.{" "}
          </NavLink>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
