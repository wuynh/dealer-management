import React, { useState, useEffect } from "react";
import "./styles.css";

function ModelList() {
  const [models, setModels] = useState([]);

  const fetchdata = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/models/");
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        console.log("error");
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchdata();
  }, []);

  return (
    <div className="mt-4">
      <h1 className="list">Models</h1>
      <table className="table table-image">
        <thead className="table-primary text-center">
          <tr className="align-middle text-center">
            <th scope="col"></th>
            <th scope="col">Name</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model, index) => (
            <tr className="align-middle text-center" key={model.id}>
              <th scope="row">{index + 1}</th>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                <img
                  src={model.picture_url}
                  className="rounded mx-auto d-block"
                  style={{ width: "300px", height: "200px" }}
                  alt=""
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ModelList;
